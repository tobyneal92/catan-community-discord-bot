import motor.motor_asyncio
import json
import urllib.parse

def get_db(production = False):
    with open('../secret.json') as f:
        config = json.load(f)

    cluster = motor.motor_asyncio.AsyncIOMotorClient(
        f'mongodb+srv://CCOpenDev:{urllib.parse.quote_plus(config["db"]["password"])}'
        f'@ccdevcluster0.ndot3.mongodb.net/CCDevDB0?retryWrites=true&w=majority')

    if not config["db"].get("database") or production:
        db = cluster.get_default_database()
    else:
        db = cluster[config["db"]["database"]]
    return db

if __name__=="__main__":
    pass