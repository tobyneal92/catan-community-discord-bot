import db as database
import asyncio
import csv
import functions


def id_from_username(username):
    return username[2:-1]


async def fix_names():
    db = database.get_db(True)

    state = functions.BotState(db)

    id_mapping = {}
    with open("Members - Blad1.csv", encoding='utf-8') as f:
        data = csv.DictReader(f, fieldnames=("name", "id", "nick", "role"))

        for row in data:
            id_mapping[row["id"]] = row["name"]
    print(id_mapping)

    users = db["users"].find({
        "display_name": ""
    })
    async for user in users:
        print(user["discord_username"], user["display_name"], user["lp"])
        discord_id = id_from_username(user["discord_username"])
        if discord_id not in id_mapping:
            print("Error processing " , discord_id)
            continue
        print(id_mapping[discord_id])
        result = await db["users"].update_one(
            {
                "_id": user["_id"]
            },
            {
                "$set": {
                    "display_name": id_mapping[discord_id]
                }
            }
        )

        if (result.modified_count != 1):
            print("Failed at " + id_mapping[discord_id])
        pass
        # user_id =


if __name__ == "__main__":
    asyncio.run(fix_names())
